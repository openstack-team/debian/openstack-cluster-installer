class oci::pxe_server(
){

  class {'::tftp':
    directory => '/var/lib/openstack-cluster-installer/tftp',
    options   => '--secure -v -v -v',
  }
  exec { 'oci-root-ca-gen':
    command => '/usr/bin/oci-root-ca-gen',
    creates => '/etc/openstack-cluster-installer/pki/ca/root-ca.crt',
  }

  class { 'postfix':
    inet_interfaces     => 'localhost',
    inet_protocols      => 'ipv4',
  }
}
