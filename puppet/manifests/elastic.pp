class oci::elastic(
  $machine_hostname         = undef,
  $machine_ip               = undef,
  $self_signed_api_cert     = true,

  $elastic_repo_url         = 'https://artifacts.elastic.co/packages',
  $elastic_version          = 7,
  $elastic_key_url          = 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
  $elastic_key_id           = '46095ACC8548582C1A2699A9D27D666CD88E42B4',

  $pass_cloudkitty_elastic  = undef,
  $pass_elastic_keystore    = undef,

  # From variables.json
  $kernel_from_backports    = false,
){

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  include ::java
  class { '::elastic_stack::repo':
    version       => $elastic_version,
    base_repo_url => $elastic_repo_url,
    repo_key_url  => $elastic_key_url,
    repo_key_id   => $elastic_key_id,
  }
  class { '::elasticsearch':
    ssl            => true,
    ca_certificate => '/etc/ssl/certs/oci-pki-oci-ca-chain.pem',
    certificate    => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
    private_key    => '/etc/ssl/private/ssl-cert-snakeoil.key',
    keystore_password => $pass_elastic_keystore,
    config => {
      'network.bind_host'    => '0.0.0.0',
      'discovery.seed_hosts' => $machine_hostname,
      'discovery.type'       => 'single-node',
    }
  }
  -> ::elasticsearch::user { 'cloudkitty':
    password => $pass_cloudkitty_elastic,
    roles    => ['superuser'],
  }
  -> exec { 'create-cloudkitty-template':
    command => join(['/usr/bin/curl -XPOST "https://', $machine_hostname, ':9200/_template/cloudkitty" -H "kbn-xsrf: reporting" -H "Content-Type: application/json" -d \'{
  "index_patterns": ["*"],
  "settings": {
    "index": {
      "number_of_shards": "1",
      "number_of_replicas": "1"
    }
  },
  "mappings": {
    "properties": {
      "end": {
        "type": "date",
        "ignore_malformed": true
      },
      "groupby": {
        "dynamic": "true",
        "properties": {
          "id": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "project_id": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "user_id": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      },
      "metadata": {
        "dynamic": "true",
        "properties": {
          "container_format": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "disk_format": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "flavor_id": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "flavor_name": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "vcpus": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "volume_type": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      },
      "price": {
        "type": "double",
        "ignore_malformed": true
      },
      "qty": {
        "type": "double",
        "ignore_malformed": true
      },
      "start": {
        "type": "date",
        "ignore_malformed": true
      },
      "type": {
        "type": "keyword"
      },
      "unit": {
        "type": "keyword"
      }
    }
  },
  "aliases": {}
}\''], '')
  }
  -> exec { 'create-cloudkitty-index':
    command => join(['/usr/bin/curl -XPOST https://', $machine_hostname, ':9200/_bulk -H "kbn-xsrf: reporting" -H "Content-Type: application/json" -d\'{ "index" : { "_index" : "cloudkitty", "_id" : "1" } } { "type" : "create" }\''], ''),
  }
}
