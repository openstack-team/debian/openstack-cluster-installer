class oci::haproxy(
){
  # First, we accept binding on non-local IPs:
#  sysctl::value { 'net.ipv4.ip_nonlocal_bind':
#    value => "1",
#    target => '/etc/sysctl.d/ip-nonlocal-bind.conf',
#  }->
  file { "/etc/haproxy/ssl":
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    selinux_ignore_defaults => true,
    require                 => Package['haproxy'],
  }->
  file { "/etc/haproxy/ssl/private":
    ensure                  => directory,
    owner                   => 'root',
    mode                    => '0755',
    selinux_ignore_defaults => true,
  }->
  file { "/etc/haproxy/ssl/private/oci-pki-api.pem":
    ensure                  => present,
    owner                   => "haproxy",
    source                  => "/etc/ssl/private/oci-pki-api.pem",
    selinux_ignore_defaults => true,
    mode                    => '0600',
  }
  class { 'haproxy':
    restart_command => 'systemctl reload haproxy',
    global_options   => {
      'log'     => '/dev/log local0',
      'chroot'  => '/var/lib/haproxy',
      'pidfile' => '/var/run/haproxy.pid',
      'maxconn' => '40960',
      'user'    => 'haproxy',
      'group'   => 'haproxy',
      'stats'   => [
        'socket /var/lib/haproxy/stats',
        'socket /var/lib/haproxy/admin.sock mode 600 level admin',
        'timeout 30s'
      ],
      'daemon'   => '',
      'nbthread' => '8',
      'ssl-default-bind-ciphers'   => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',
      'ssl-default-bind-options'   => 'no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets',
      'ssl-default-server-ciphers' => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',
      'ssl-default-server-options' => 'no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets',
    },
    defaults_options => {
      'log'     => 'global',
      'mode'      => 'http',
      'option'    => [
          'httplog',
        ],
      'retries' => '3',
      'maxconn' => '8000',
      'monitor-uri' => '/health'
    },
    merge_options => false,
    require       => Sysctl::Value['net.ipv4.ip_nonlocal_bind'],
  }

  # Fix haproxy log to keep 7 days of logs, instead of
  # the default which is 52.
  logrotate::rule { 'haproxy':
    path          => '/var/log/haproxy.log',
    rotate        => '7',
    rotate_every  => 'day',
    missingok     => true,
    compress      => true,
    delaycompress => true,
    postrotate    => '/usr/lib/rsyslog/rsyslog-rotate',
  }
}
