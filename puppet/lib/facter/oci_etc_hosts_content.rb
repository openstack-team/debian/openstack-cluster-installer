# module_name/lib/facter/oci_etc_hosts_content.rb
Facter.add(:oci_etc_hosts_content) do
  setcode do
    # return content of foo as a string
    doprint = 1
    myvar = ''

    File.open('/etc/hosts').readlines.each{ |line|
      if line =~ /OCISTA_MAINTAINED/
        doprint = 0
      end
      if doprint == 1
        myvar << line
      end
      if line =~ /OCIFIN_MAINTAINED/
        doprint = 1
      end
    }

    myvar
  end
end
