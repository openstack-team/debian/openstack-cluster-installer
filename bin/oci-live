#!/bin/sh

set -e
set -x

if ! [ -r /etc/openstack-cluster-installer/openstack-cluster-installer.conf ] ; then
	echo "Could not load /etc/openstack-cluster-installer/openstack-cluster-installer.conf"
	echo "Copy it from your OCI install if you're trying to build a foreign arch live image."
	exit 1
else
	TMPFILE=$(mktemp -t openstack-cluster-installer.XXXXXX)
	cat /etc/openstack-cluster-installer/openstack-cluster-installer.conf | grep -v '^\[' >${TMPFILE}
	. ${TMPFILE}
	rm ${TMPFILE}
fi

sshi () {
	local HOST CMD
	HOST="${1}"
	CMD="${2}"
	SSH_AUTH_SOCK= ssh -i /etc/openstack-cluster-installer/id_rsa -o ConnectTimeout=5 root@${HOST} "${CMD}"
}

scpi () {
	local HOST SRC DST
	HOST="${1}"
	SRC="${2}"
	DST="${3}"
	SSH_AUTH_SOCK= scp -r -i /etc/openstack-cluster-installer/id_rsa -o ConnectTimeout=5 ${SRC} root@${HOST}:${DST}
}

scpir () {
	local HOST SRC DST
	HOST="${1}"
	SRC="${2}"
	DST="${3}"
	SSH_AUTH_SOCK= scp -i /etc/openstack-cluster-installer/id_rsa -o ConnectTimeout=5 root@${HOST}:${SRC} ${DST}
}

mkdir -p /var/lib/openstack-cluster-installer/tftp/live
if [ -n "${image_builder_hosts}" ] ; then
	for BUILDER in ${image_builder_hosts} ; do
		echo "===> Building image in $BUILDER"
		sleep 1
		echo "-> Copying config files"
		sshi $BUILDER "mkdir -p /etc/openstack-cluster-installer"

		# Manage /etc/openstack-cluster-installer/id_rsa
		if ! [ -e /etc/openstack-cluster-installer/id_rsa ] ; then
			ssh-keygen -P '' -f /etc/openstack-cluster-installer/id_rsa
		fi

		##################################################################
		### Create the SSH root ca for signing the live image host key ###
		##################################################################
		# Create the ssh CA key, which we push into the image, so the
		# image can sign host keys on boot. This isn't very safe to do
		# that, but that's still safer than randomly trusting SSH host keys
		# of new machines.
		mkdir -p /etc/openstack-cluster-installer/live-image-ssh-host-key
		if ! [ -e /etc/openstack-cluster-installer/live-image-ssh-host-key/ca ] ; then
			ssh-keygen -P '' -f /etc/openstack-cluster-installer/live-image-ssh-host-key/ca
		fi
		# Make sure that the ca.pub is in OCI's /etc/ssh/ssh_known_hosts
		CA_PUBKEY_CONTENT=$(cat /etc/openstack-cluster-installer/live-image-ssh-host-key/ca.pub)
		if ! grep -q "${CA_PUBKEY_CONTENT}" /etc/ssh/ssh_known_hosts ] ; then
			echo "@cert-authority * ${CA_PUBKEY_CONTENT}" >>/etc/ssh/ssh_known_hosts
		fi

		# Manage the authorized_keys file
		if ! [ -e /etc/openstack-cluster-installer/authorized_keys ] ; then
			if [ -e /root/.ssh/authorized_keys ] ; then
				cp /root/.ssh/authorized_keys /etc/openstack-cluster-installer/authorized_keys
				cat /etc/openstack-cluster-installer/id_rsa.pub >>/etc/openstack-cluster-installer/authorized_keys
			else
				cp /etc/openstack-cluster-installer/id_rsa.pub /etc/openstack-cluster-installer/authorized_keys
			fi
		fi

		for i in openstack-cluster-installer.conf authorized_keys live-image-additions live-image-ssh-host-key oci-firmware-upgrade-config.json pubkey.gpg pki ; do
			scpi ${BUILDER} /etc/openstack-cluster-installer/${i} /etc/openstack-cluster-installer/
		done
		
		sshi $BUILDER "mkdir -p /root/live ; cd /root/live ; openstack-cluster-installer-build-live-image --pxe-server-ip ${OCI_IP}"
		B_ARCH=$(sshi $BUILDER "uname -m")
		INITRD_FNAME=$(sshi ${BUILDER} "ls /root/live/tftpboot/live/initrd*")
		KERNEL_FNAME=$(sshi ${BUILDER} "ls /root/live/tftpboot/live/vmlinuz*")
		case "${B_ARCH}" in
		"aarch64")
			scpir ${BUILDER} /root/live/binary/live/filesystem.squashfs /var/lib/openstack-cluster-installer/filesystem_arm64.squashfs
			scpir ${BUILDER} ${INITRD_FNAME} /var/lib/openstack-cluster-installer/tftp/live/initrd-arm64.img
			scpir ${BUILDER} ${KERNEL_FNAME} /var/lib/openstack-cluster-installer/tftp/live/vmlinuz-arm64
			scpir ${BUILDER} /usr/lib/shim/shimaa64.efi.signed /var/lib/openstack-cluster-installer/tftp/shimaa64.efi.signed
			scpir ${BUILDER} /usr/lib/grub/arm64-efi-signed/grubnetaa64.efi.signed /var/lib/openstack-cluster-installer/tftp/grubaa64.efi
		;;
		"x86_64")
			scpir ${BUILDER} /root/live/binary/live/filesystem.squashfs /var/lib/openstack-cluster-installer/filesystem.squashfs
			scpir ${BUILDER} ${INITRD_FNAME} /var/lib/openstack-cluster-installer/tftp/live/initrd.img
			scpir ${BUILDER} ${KERNEL_FNAME} /var/lib/openstack-cluster-installer/tftp/live/vmlinuz
			scpir ${BUILDER} /usr/lib/shim/shimx64.efi.signed /var/lib/openstack-cluster-installer/tftp/shimx64.efi.signed
			scpir ${BUILDER} /usr/lib/grub/x86_64-efi-signed/grubnetx64.efi.signed /var/lib/openstack-cluster-installer/tftp/grubx64.efi
		;;
		*)
			echo "Not supported yet: please contribute another arch here..."
			exit 1
		;;
		esac
	done
fi
echo "===> Building image locally"
mkdir -p /root/live
cd /root/live
openstack-cluster-installer-build-live-image
