[network]
# Address of your OpenStack cluster, CIDR notation
OPENSTACK_CLUSTER_NETWORK=192.168.100.0/24

# Name of the NIC that will serve DHCP + PXE
# for installing Debian
PXE_NIC_NAME=osinstallnic0

# User holding the NIC
PXE_VM_NIC_USER=zigo

# name of the tap interface
PXE_VM_VIRTAP_NAME=osinstalltap

# bridge name
PXE_BRIDGE_NAME=osinstallbr

# qemu VM MAC
QEMU_VM_MAC=08:00:27:06:CC:DF

# URL of your debian proxy/mirror
# Replace it with your closest mirror.
# Example: http://mirror.infomaniak.com/debian
debian_mirror=http://deb.debian.org/debian

# URL of your debian security proxy/mirror
# Replace it with your closest security mirror.
# Example: like http://mirror.infomaniak.com/debian-security
debian_security_mirror=http://security.debian.org/debian-security

# Package containing keyring used to sign packages in above repositories
# this is useful when using self-hosted package repos which are not signed
# by the official Debian archive keyring
# Leave empty when using official debian packages
debian_keyring_package=

# Filename of keyring installed by above package. This also needs to be availble
# on the OCI server.
# Leave empty when using official debian packages
debian_keyring_file=

# Whether to install the keyring package specified above on OpenStack nodes
install_debian_keyring_package=no

# Whether to copy above keyring file to OpenStack nodes. Using a package instead
# is preferred
install_debian_keyring_file=no

# URL of the incoming buildd repo: useful for Sid development of OCI.
debian_incoming_buildd=http://incoming.debian.org/debian-buildd

# Ceph repository, needed for Stretch, as we need Luminous from upstream.
# Normal repo without mirroring is: http://download.ceph.com/debian-luminous
# Infomaniak mirror is: http://apt.infomaniak.ch/download.ceph.com/debian-luminous
# Mirror list available at: https://ceph.com/get/#ceph-mirrors
debian_mirror_ceph=http://download.ceph.com/debian-luminous

# Should we set http_proxy, for example in oci-poc-provision-cloud
# when downloading the Debian image?
USE_HTTP_PROXY=no

# http_proxy value to use when doing wget in the
# oci-poc-provision-cloud script.
HTTP_PROXY_ADDR=example.com:3128

# Address of the OCI web server that the hardware discovery agent
# will contact, and also the IP address of the puppet-master, as per
# slave nodes /etc/hosts file.
OCI_IP=192.168.111.1

# These are the networks allowed to query the OCI web site
# without authentication, ie: machines that do PXE boot.
# Typically, this is your DHCP network.
# Every network that you will add in OCI will be trusted too,
# so that machines can report their status ie: live, installed, etc..
TRUSTED_NETWORKS=192.168.100.0/24,10.54.240.0/24,10.52.240.0/24

[radius]
# Should the auth system use radius?
use_radius=no

# Address of your Radius authentication server.
server_hostname=localhost

# Shared secret to contact your Radius server.
shared_secret=changeme

[database]
connection=mysql+pymysql://oci:43FAnaQHKizfIrBMksqITw@localhost:3306/oci

[dellipmi]
# If the chassis vendor is Dell, should the Dell iDRAC
# utilities be installed on the target?
target_install_dell_ipmi=no

# Should the Dell iDRAC IPMI utilities be installed on the Live image?
live_image_install_dell_ipmi=no

# Address of the Dell IPMI utilities. At Infomaniak, we
# have a copy of them here:
# http://apt.infomaniak.ch/linux.dell.com/repo/community/openmanage/910/stretch
dell_ipmi_repo=https://linux.dell.com/repo/community/openmanage/910/stretch

[hpe]
# If the chassis vendor is HPE, should the HPE
# utilities be installed on the target?
target_install_hpe=no

# Should the HPE hponcfg, etc. tools be installed on the Live image?
live_image_install_hpe=no

# # Address of the HPE utilities. At Infomaniak, we
# have a copy of them here:
# http://apt.infomaniak.ch/downloads.linux.hpe.com/SDR/repo/mcp/debian/
hpe_repo=http://downloads.linux.hpe.com/SDR/repo/mcp/debian

# Override distro suite by something custom. Useful if not available
# for example when setting-up testing.
hpe_force_debian_suite=yes

# What distro suite to use if using hpe_force_debian_suite set to yes.
hpe_debian_suite=buster

# Should we automatically install iLo license with ilorest?
hpe_ilo_install_license=no

# What's your iLo license
#hpe_ilo_license=XXXXX-XXXXX-XXXXX-XXXXX-XXXXX

[perccli]
# If the chassis vendor is using perccli,
# sould we install it on the target?
target_install_perccli=no

# Should perccli be installed on the Live image?
live_image_install_perccli=no

# Address of the perccli repository. Since we have no rights to
# redistribute them, you are on your own to make your own.
perccli_repo=http://apt.infomaniak.ch/infomaniak

# Override distro suite by something custom. Useful if not available
# for example when setting-up testing.
perccli_force_debian_suite=yes

# What distro suite to use if using perccli_force_debian_suite set to yes.
perccli_debian_suite=bullseye

[megacli]
# If your vendor is using an LSI card, then you probably
# want to have megacli installed.
target_install_megacli=no

# Should megacli be also installed in the live image?
live_image_install_megacli=no

# Address of the megacli repository. At infomaniak, we use a
# mirror, but you can also use approx like this:
# http://apt.infomaniak.ch/hwraid.le-vert.net/debian/
megacli_repo=http://hwraid.le-vert.net/debian

# Force a given suite name rather than the one defined
# as debian_release=
megacli_force_debian_suite=no

# What suite to use if force_debian_suite is set to yes?
megacli_debian_suite=sid

# Should we automatically clear the MegaCli configuration after N
# run of the hardware discovery?
megacli_auto_clear=no

# After how many hardware discovery run should we run MegaCli auto-clear?
megacli_auto_clear_num_of_discovery=3

# Should we automatically apply the MegaCli profile after N run of
# the hardware discovery?
megacli_auto_apply=no

# After how many hardware discovery should we run MegaCli auto-apply?
megacli_auto_apply_num_of_discovery=7

[onecli]
# Install onecli in production systems?
target_install_onecli=no

# Install onecli in the live image?
live_image_install_onecli=no

# Address of the onecli repo. Note that this is not an official
# package, that we repacked, and have no rights to redistribute.
# So you'll have to make your own onecli deb package...
onecli_repo=http://apt.infomaniak.ch/infomaniak

# Force a given suite name rather than the one defined
# as debian_release=
onecli_force_debian_suite=yes

# What suite to use if force_debian_suite is set to yes?
onecli_debian_suite=bullseye

[live_image]
# In our configuration, the 10 Gbits/s cards are detected first, and
# then, the Debian live tries to do the DHCP on these cards first,
# which then fails. Therefore, a nice hack is to remove these drivers
# from the live image's initrd but leave it within the squashfs. This
# way, the DHCP phase before the wget of the squashfs will not use these
# cards as the driver isn't present.
# Note that this may not be useful if your switch is capable of doing
# lacp-bypass, which Cumulus switches can do.
remove_drivers_from_initrd=no

# List of drivers to remove when building the live image
remove_drivers_from_initrd_list="broadcom/bnx2.ko broadcom/bnx2x/bnx2x.ko intel/ixgbe/ixgbe.ko"

# Syslinux has 2 modes: one which is graphical and displays a nice
# splash screen, and one which is serial console compatible, which
# is text only, without the nice splash screen. Select here which
# one you want to use in the live image.
force_syslinux_text_menu=yes

# Should we install stable-backports in the Live image?
live_image_setup_backports_repo=no

# Should we install the contrib and non-free repositories?
live_image_setup_nonfree_repo=no

# Should we install non-free firmware shipped in the Debian non-free
# repository?
live_image_install_nonfree_firmware_from_backports=yes

# List of non-free firmware to install from backports.
live_image_install_firmware_from_backports_list="firmware-linux-nonfree firmware-bnx2 firmware-bnx2x firmware-qlogic"

# Should we install the Linux kernel from backports?
# The live_image_setup_backports_repo option must be set
# to yes for this to work.
live_image_install_kernel_from_backports=yes

# List here all the image builder hosts. OCI will ssh them
# to build images of foreign arch, like arm64.
image_builder_hosts=

[production_system]
# Should we install stable-backports in the production systems?
production_system_setup_backports_repo=no

# Should we install the contrib and non-free repositories?
production_system_setup_nonfree_repo=no

# Should we install non-free firmware shipped in the Debian non-free
# repository?
production_system_install_nonfree_firmware_from_backports=yes

# List of non-free firmware to install from backports.
production_system_install_firmware_from_backports_list="firmware-linux-nonfree firmware-bnx2 firmware-bnx2x firmware-qlogic"

[releasenames]
# With the default configuration, OCI will use just plain
# Debian with any backports. It is strongly advised to
# use the non-official backports.

# Name of the OpenStack release when building the image
openstack_release=victoria

# Name of the Debian release when building the image
debian_release=bullseye

# Should we use a Debian stable unofficial repository?
use_debian_dot_net_backport=no

# Install official Debian backport repository?
use_debian_official_backports=no

# Install osbpo Ceph backport repo?
ceph_use_osbpo=no

# Ceph release name in osbpo.
ceph_osbpo_release=ceph-reef

# Pin Ceph from Debian official backports or osbpo?
pin_ceph_from_stable_backports=no

# This is useful for development, when you want fast
# update of the package in the Sid Debian repository.
# Not to be used in production.
install_buildd_incoming=no

# This option is not advised, because the upstream Ceph
# Debian repositories are not well enough supported ie:
# upstream is not doing a good enough job, and has dropped
# support for Debian Stable during the life of Stretch/Buster.
install_ceph_upstream_repo=no

[ssh]
# If set to yes, will use the PHP extension
# otherwise, use PHP exec. Recommendation is to set this to no.
use_php_ssh2=no

[ipmi]
# Set this to yes if you want OCI to automatically assign IPMI
# addresses to your machines during the hardware recovery process.
# To do this, a network with role IPMI must be created using:
# ocicli network-create ipmi-network-1 192.168.103.0 24 bdb-zone-1 no
# ocicli network-set ipmi-network-1 --role ipmi --ipmi-match-addr 192.168.100.0 --ipmi-match-cidr 24
# The --ipmi-match-addr and --ipmi-match-cidr must match the DHCP address of discovered machines.
# Note that passwords will be picked-up randomly.
automatic_ipmi_numbering=no

# Set this to the default IPMI username you wish to see
# automatically provisionned. It is strongly advised to set
# this not to the default value, just to make it less easy
# to guess.
automatic_ipmi_username=oci

[tempest]
# Name of the external network.
# Note that in many cases, ext-net1 wont work, simply
# because tempest isn't made for it.
external_network_name=ext-floating1
floating_network_name=ext-floating1

# Name of the flavor used to run the tests
flavor_name=cpu1-ram2-disk5
flavor_alt_name=cpu2-ram6-disk20

[glance_image]
# This image can be used to setup the image automatically in
# the cluster at setup time. Will only be used if it's available.
# This is mostly useful for doing tempest testing automatically,
# together with the openstack-cluster-installer-poc package.
image_path=/root
image_name=debian-12-generic-amd64.qcow2

image_ssh_user=debian

[dns_plugin]
# Should OCI call a script to update DNS information for a host?
# This will be done when a machine is added to a cluster.
call_dns_shell_script=no

# Path to the script to update DNS information for a host.
dns_create_script_path=/usr/local/bin/oci-dns-create-host
dns_delete_script_path=/usr/local/bin/oci-dns-delete-host

[root_pass_plugin]
# Should OCI call a script to save the root password when it is
# changed on a server? This may be usefull to save it in for
# example Hashicorp VAULT.
# This will be called when the server OS is reported as installed.
call_root_password_change=no

# Path to the root password plugin script.
root_password_create_script_path=/usr/local/bin/oci-root-password-create-host
root_password_delete_script_path=/usr/local/bin/oci-root-password-delete-host

[ipmi_pass_plugin]
# Should OCI call a script to save the IPMI passwords? This may
# be usefull to save it in for example OpenBao / Hashicorp VAULT.
call_ipmi_password_change=no
ipmi_password_create_script_path=/usr/local/bin/oci-ipmi-password-create-host
ipmi_password_delete_script_path=/usr/local/bin/oci-ipmi-password-delete-host

[monitoring_plugin]
# Should OCI calla script to update the monitoring information
# when a host is set into production?
# This will be called when the puppet run is reported as successful.
call_monitoring_plugin=no

# Path to the script to update the monitoring information.
monitoring_create_script_path=/usr/local/bin/oci-monitoring-create-host
monitoring_delete_script_path=/usr/local/bin/oci-monitoring-delete-host

[auto_provision]
# Should we auto-add a machine to a cluster?
auto_add_machines_to_cluster=no

# When a machine is automatically added, to which cluster should
# it belong?
auto_add_machines_cluster_name=

# After how many discovery run should we auto-add machines to the cluster?
auto_add_machines_num_of_discovery=9

[auto_racking]
# Should we automatically guess racking information based on the
# switch name and what is in auto-racking.json?
auto_rack_machines_info=yes

# How many runs of hardware discovery before auto-racking?
auto_rack_machines_num_of_discovery=7

[auto_install_os]
# Should machines install the OS automatically?
auto_install_machines_os=no

# After how many runs of the hardware discovery should the
# servers be installed?
auto_install_machines_num_of_discovery=15

[filebeat]
# Repository URL for Filebeat.
# Our mirror: http://apt.infomaniak.ch/artifacts.elastic.co/packages/6.x/apt
filebeat_repo=https://artifacts.elastic.co/packages/7.x/apt

[elastic]
# Repository URL for Elastic search.
# Our mirror: http://apt.infomaniak.ch/artifacts.elastic.co/packages
elastic_repo_url=https://artifacts.elastic.co/packages

# Version use for Elastic
elastic_version=7

# Elastic repo GPG key
# Our mirror: http://apt.infomaniak.ch/artifacts.elastic.co/GPG-KEY-elasticsearch
elastic_key_url=https://artifacts.elastic.co/GPG-KEY-elasticsearch

# Key ID for the repo
elastic_key_id=46095ACC8548582C1A2699A9D27D666CD88E42B4

[foreman]
# URL of the foreman API, like https://foreman.example.com/api/v2
FOREMAN_URL=https://poc-katello.infomaniak.ch/api/v2

# Username for the foreman API.
FOREMAN_USERNAME=admin

# Password for the foreman API.
FOREMAN_PASSWORD=mypassword

# Should we use -k with curl to query foreman.
INSECURE="-k"
