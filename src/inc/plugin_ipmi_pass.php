<?php

function machine_save_ipmi_pass($con, $conf, $machine){
    if($conf["ipmi_pass_plugin"]["call_ipmi_password_change"]){
        $script_path = $conf["ipmi_pass_plugin"]["ipmi_password_create_script_path"];

        if(file_exists ($script_path) && is_executable ($script_path) ){
            $cmd = "$script_path --hostname ".$machine["hostname"]." --ipmi-username ".escapeshellarg($machine["ipmi_username"])." --ipmi-password " . escapeshellarg($machine["ipmi_password"]) . " --ipmi-ip " . $machine["ipmi_addr"];
            $output = array();
            $return_var = 0;
            exec($cmd, $output, $return_var);
        }else{
            return FALSE;
        }
    }else{
        return FALSE;
    }
}

function machine_forget_ipmi_pass($con, $conf, $machine){

    if($conf["ipmi_pass_plugin"]["call_ipmi_password_change"]){

        $script_path = $conf["ipmi_pass_plugin"]["ipmi_password_delete_script_path"];
        if(file_exists ($script_path) && is_executable ($script_path) ){
            $cmd = "$script_path --hostname ".$machine["hostname"];
            $output = array();
            $return_var = 0;
            exec($cmd, $output, $return_var);
        }else{
            return FALSE;
        }
    }else{
        return FALSE;
    }
}

?>
